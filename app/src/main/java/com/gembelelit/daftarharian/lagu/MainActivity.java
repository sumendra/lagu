package com.gembelelit.daftarharian.lagu;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    MediaPlayer rindik1;
    MediaPlayer rindik2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void startrepeat(View view) {
        rindk1.run();
    }

    public void stoprepeat(View view) {
        mHandler.removeCallbacks(rindk1);
        mHandler.removeCallbacks(rindk2);
    }
    private Runnable rindk1 = new Runnable() {
        @Override
        public void run() {
            rindik1 = MediaPlayer.create(MainActivity.this , R.raw.su1);
            rindik1.start();
           mHandler.postDelayed(this, 2000);
        }
    };
    private Runnable rindk2 = new Runnable() {
        @Override
        public void run() {
            rindik2 = MediaPlayer.create(MainActivity.this, R.raw.su2);

            rindik2.start();
            mHandler.postDelayed(this, 2400);
        }
    };

    public void repeatrindik(View view){
        rindk2.run();
    }
}
